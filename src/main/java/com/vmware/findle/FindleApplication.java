package com.vmware.findle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindleApplication {

	public static void main(String[] args) {
		SpringApplication.run(FindleApplication.class, args);
	}

}
